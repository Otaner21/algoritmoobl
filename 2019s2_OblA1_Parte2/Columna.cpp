#ifndef  COLUMNA_CPP
#define COLUMNA_CPP

#include "Columna.h"

Columna::Columna()
{
	assert(false);
	
}
Columna::Columna(Cadena nombreCol,CalifCol calif)
{
	nombre = nombreCol;
	calificador = calif;
	datos = new ListaPosImp<Cadena>();
}
	
Columna::Columna(const Columna& clna)
{
	*this = clna;
}

Columna::~Columna()
{
	delete datos;
}

Columna& Columna::operator=(const Columna & clna)
{
	if (this != &clna) {
		nombre = clna.nombre;
		calificador = clna.calificador;
		datos = new ListaPosImp<Cadena>();
		*(datos) = *(clna.datos);
		}
	return *this; // TODO: insertar una instrucci�n return aqu�
}

bool Columna::operator==(const Columna clna) const
{
	return nombre == clna.nombre;
}

bool Columna::operator<=(const Columna& clna) const
{
	return nombre <= clna.nombre;
}

bool Columna::operator>=(const Columna& c) const
{
	return nombre >= c.nombre;
}

bool Columna::operator!=(const Columna& c) const
{
	return nombre != c.nombre ;
}

bool Columna::operator<(const Columna& c) const
{
	return nombre < c.nombre;
}

bool Columna::operator>(const Columna& c) const
{
	return nombre > c.nombre;
}

bool Columna::ExisteDto(const Cadena& cdna) const
{

	return datos->Existe(cdna);
}

const Cadena& Columna::GetNombre() const
{
	return this->nombre;// TODO: insertar una instrucci�n return aqu�
}

const CalifCol& Columna::GetCalificador() const
{
	return this->calificador;
}

const Cadena& Columna::GetDatoenpos(unsigned int pos) const
{
	return datos->ElementoPos(pos);// TODO: insertar una instrucci�n return aqu�
}

unsigned int Columna::PosicionDato(const Cadena& cdna) const
{
	return datos->Posicion(cdna);
}

unsigned int Columna::VecesExiste(const Cadena& cdna) const
{
	unsigned int res = 0; 
	for (int i = 1; i <= datos->CantidadElementos(); i++) {
		if (cdna == datos->ElementoPos(i-1)) {
			res++;
		}
	}
	return res;
}

unsigned int Columna::TamanioColumna() const
{
	return datos->CantidadElementos();
}

void Columna::AgregarDatos(Cadena& dato)
{
	datos->AgregarFin(dato);
}

void Columna::BorrarDatoPos(unsigned int pos)
{
	this->datos->BorrarPos(pos);
}


#endif

ostream& operator<<(ostream& out, const Columna& clna)
{
	out << clna.GetNombre();
	return out;
	// TODO: insertar una instrucci�n return aqu�
}
