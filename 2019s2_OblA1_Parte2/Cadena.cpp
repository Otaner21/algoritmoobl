
#include "Cadena.h"


#ifndef CADENA_CPP
#define CADENA_CPP

/****************************************************/
ostream &operator<<(ostream &os, const Cadena &cad){
	if(cad.s)
		os << cad.s;
	return os;
}

istream &operator>>(istream &is, Cadena &cad){
	char aux[200];
	is>>aux;
	cad = aux;
	return is;
}

/****************************************************/
Cadena::Cadena(const char *cad, bool ignoreCase) {
	size_t len = strlen(cad)+1;
	s = new char[len];
	strcpy_s(s, len, cad);
	this->ignoreCase = ignoreCase;
}
/****************************************************/
Cadena::Cadena(){
	s = new char[1];
	s[0] = '\0';
	this->ignoreCase = true;
}
/****************************************************/   
Cadena::Cadena(const Cadena &c) {
	s = NULL;
	*this = c;
}
/****************************************************/
Cadena::~Cadena() {
	if(s)
		delete[] s;
}
/****************************************************/
Cadena &Cadena::operator=(const Cadena &c) {
	if(this != &c) {
		delete [] s;

		size_t len = strlen(c.s)+1;
		s = new char[len];
		strcpy_s(s, len, c.s);

		this->ignoreCase = c.ignoreCase;
	}
	return *this;
}
/****************************************************/
Cadena Cadena::operator+(const Cadena &c) const {
	size_t len = strlen(s)+strlen(c.s)+1;
	char *sNew = new char[len];
	strcpy_s(sNew, len, s);
	strcat_s(sNew, len, c.s);
	Cadena cNew(sNew, ignoreCase);
	delete [] sNew;

	return cNew;
}
/****************************************************/
bool Cadena::operator==(const Cadena &c) const {
	if (ignoreCase) 
		return _stricmp(s, c.s) == 0;
	else
		return strcmp(s, c.s) == 0;
}
/****************************************************/
bool Cadena::operator!=(const Cadena &c) const {
	if (ignoreCase) 
		return _stricmp(s, c.s) != 0;
	else
		return strcmp(s, c.s) != 0;
}
/****************************************************/
bool Cadena::operator<(const Cadena &c) const {
	if (ignoreCase) 
		return _stricmp(s, c.s) < 0;
	else
		return strcmp(s, c.s) < 0;
}
/****************************************************/
bool Cadena::operator>(const Cadena &c) const {
	if (ignoreCase) 
		return _stricmp(s, c.s) > 0;
	else
		return strcmp(s, c.s) > 0;
}
/****************************************************/
bool Cadena::operator<=(const Cadena &c) const {
	if (ignoreCase) 
		return _stricmp(s, c.s) <= 0;
	else
		return strcmp(s, c.s) <= 0;
}
/****************************************************/
bool Cadena::operator>=(const Cadena &c) const {
	if (ignoreCase) 
		return _stricmp(s, c.s) >= 0;
	else
		return strcmp(s, c.s) >= 0;
}
/****************************************************/
char *Cadena::GetNewCharPtr() const {
	size_t len = strlen(s)+1;
	char *ret = new char[len];
	strcpy_s(ret, len, s);
	return ret;
}
/****************************************************/

unsigned int Cadena::Length() const {
	return (unsigned int)strlen(s);
}

bool Cadena::Contains(const Cadena &substr) const {
	if (substr.Length() == 0)
		return true;
	char * s2 = substr.s;
	unsigned int j, j2;

	std::size_t largoS = strlen(s);
	std::size_t largoS2 = strlen(s2);
	if (!ignoreCase) {
		for (unsigned int i = 0; i < largoS; i++) {
			if (i + largoS2 > largoS + 1)
				return false;
			if (s[i] == s2[0]) {
				j = i;
				j2 = 0;
				while (j2 < largoS2 && s[j] == s2[j2]) {
					j++;
					j2++;
				}
				if (j2 == largoS2)
					return true;
			}
		}
	}
	else {
		for (unsigned int i = 0; i < largoS; i++) {
			if (i + largoS2 > largoS + 1)
				return false;
			if (toupper(s[i]) == toupper(s2[0])) {
				j = i;
				j2 = 0;
				while (j2 < largoS2 && toupper(s[j]) == toupper(s2[j2])) {
					j++;
					j2++;
				}
				if (j2 == largoS2)
					return true;
			}
		}
	}
	return false;
}

char Cadena::operator[] (const unsigned int index)
{
	assert (index < Length());
	return s[index];
}

unsigned int Cadena::CantidadsubCadenas() const
{
	unsigned res = 0;
	int i = 0;
	char delimitador[] = ":";
	while (s[i] != NULL) {
		if (s[i] == delimitador[0]) {
			res++;
		}
		i++;
	}
	//mas uno que es las ultima cadena que no tiene delimitador
	return res + 1;
	
}

 Cadena Cadena::AuxStrtok(int pos,char * delimitador)
 {
	 
	int len = strlen(s)+1;
	char* copias = new char[len];
	 strcpy_s(copias,len, s);
	 char* next = NULL;
	int ctdr = 0;
	//char delimitador[] = ":";
	Cadena empty;
	char* aux = NULL;
	int i = 0;
	bool ok = false;
	bool salir = false;
	// chequeo si la posicion de la cadena que me pide es vacia
	while (copias[i] != NULL && !salir) {
		if (copias[i] != delimitador[0]) {
			ok = false;
		}
		else {
			ctdr++;
			if (ok == true) { // si pasa 2 veces consecuitivas en true quiere decir que "::"
				if (ctdr == pos) {
					
					return empty;
				}
				pos--; // si hay un "::" quiere decir que tengo un "token/dato" menos para contar cuando pase la siguien while
			}
			else {
				ok = true;
			}
		}
		if (ctdr >= pos) { // si ya me pase de la posicion que quiero chequear salgo del while
			salir = true;
		}
		i++;
	}
	//reinicio el contador 
	ctdr = 1;
	// devuelvo la cadena en la posicion que me piden
	char* token = strtok_s(copias, delimitador,&next);
	if (token != NULL) {
		while (token != NULL && ctdr<=pos ) {
			
			if (ctdr == pos) {
				if (aux == NULL) {
					aux = token;
				}
				Cadena nueva(aux, ignoreCase);
				delete[] copias;
				return nueva;
			}
			

			char* token = strtok_s(NULL, delimitador,&next);
			 aux = token;
		
			ctdr++;
		}
	}
	return 0;
}




#endif