#include "Tabla.h"

#ifndef TABLA_CPP
#define TABLA_CPP

ostream& operator<<(ostream& out, const Tabla& t) {
	out << t.GetNombre();
	return out;
}

Tabla::Tabla() {

	// NO IMPLEMENTADA
	assert(false);
}

Tabla::Tabla(Cadena& nombreTabla) {
	columnas = new ListaPosImp<Columna>();
	nombre = nombreTabla;
	tienePK = false;
	tieneTuplas = false;
}

Tabla::Tabla(const Tabla& t) {
	//columnas = new ListaOrdImp<Columna>(*(t.columnas));
	//this->nombre = t.nombre;

	*this = t; // copy / paste del ctor();
}

Tabla::~Tabla() {
	delete columnas;// hecho
}

Tabla& Tabla::operator=(const Tabla& t) {
	if (this != &t) {
		nombre = t.nombre;
		columnas = new ListaPosImp<Columna>();
		*(columnas) = *(t.columnas);
		tienePK = t.tienePK;
		tieneTuplas = t.tieneTuplas;
		//  *(milista) = * (t.milista)
	}
	return *this;
}

bool Tabla::operator==(const Tabla& t) const {
	return nombre == t.nombre;

}

bool Tabla::operator!=(const Tabla& t) const {

	return nombre != t.nombre;
}

bool Tabla::operator<(const Tabla& t) const {
	// NO IMPLEMENTADA
	return nombre < t.nombre;
}

bool Tabla::operator>(const Tabla& t) const {
	// NO IMPLEMENTADA
	return  nombre > t.nombre; ;
}

bool Tabla::operator<=(const Tabla& t) const {

	return nombre <= t.nombre;
}

bool Tabla::operator>=(const Tabla& t) const {
	// NO IMPLEMENTADA
	return nombre >= t.nombre;
}

bool Tabla::sonIguales(const Tabla& t) const
{
	// NO IMPLEMENTADA
	return false;
}

const Cadena& Tabla::GetNombre() const {
	// NO IMPLEMENTADA
	return  this->nombre;//*(new Cadena()); // Esta linea no es correcta, no deber�a hacer new. Esta asi para que no de warning al no estar implementada.
}

TipoRetorno Tabla::addCol(Cadena& nombreCol, CalifCol calificadorColumna) {
	Columna col(nombreCol, calificadorColumna);
	if (this->tienePK && calificadorColumna == 2) {
		cout << "ERROR: No se puede agregar la columna, la tabla ya tiene una columna PK." << endl;
		return ERROR;
	}
	if (columnas->Existe(col)) {
		cout << "ERROR: No se puede agregar la columna, nombreCol ya existe." << endl;
		return ERROR;
	}
	if (calificadorColumna == 2) {
		this->tienePK = true;
	}
	else {
		if (this->tieneTuplas && calificadorColumna != 1) {
			cout << "ERROR: No se puede agregar la columna, la tabla ya tiene al menos una tupla y el calificador no es EMPTY." << endl;
			return ERROR;
		}
		if (this->tieneTuplas && calificadorColumna == 1) {
			//CARGAR TODA LA NUEVA COLUMNA CON  �@EMPTY@�
		}
	}
	columnas->AgregarFin(col);
	return OK;

}



TipoRetorno Tabla::delCol(Cadena& nombreCol) {

	if (columnas->EsVacia()) {
		cout << "ERROR: No se puede eliminar la columna, nombreCol no existe." << endl;
		return ERROR;
	}
	else {

		bool ok = false;
		for (Iterador <Columna> i = columnas->GetIterador(); !i.EsFin(); i++) {
			if (i.Elemento().GetNombre() == nombreCol) {
				Columna clna = i.ElementoInseguro();
				columnas->Borrar(clna);
				if (columnas->CantidadElementos() != 0) {
					this->BorraTuplaRepetidas();
				}
				ok = true;
			}
		}
		if (!ok) {
			cout << "ERROR: No se puede eliminar la columna, nombreCol no existe." << endl;
			return ERROR;

		}
		if (columnas->CantidadElementos() == 0) {
			this->tieneTuplas = false;
		}
		return OK;
	}

}

TipoRetorno Tabla::insertInto(Cadena& valoresTupla) {
	unsigned int cant = valoresTupla.CantidadsubCadenas();
	if (columnas->EsVacia()) {
		cout << "ERROR: No se puede agregar la tupla, nombreTabla no tiene columnas." << endl;
		return ERROR;
	}
	if (valoresTupla.Length() == 0) {
		cout << "ERROR: No se puede agregar la tupla, valoresTupla es vacio." << endl;
		return ERROR;
	}
	if (valoresTupla.Contains("@EMPTY@")) {
		cout << "ERROR: No se puede agregar la tupla, no se puede insertar el dato @EMPTY@." << endl;
		return ERROR;
	}
	if (ExisteTupla(valoresTupla)) {
		cout << "ERROR: No se puede agregar la tupla, ya existe una tupla igual." << endl;
		return ERROR;
	}

	if (cant != columnas->CantidadElementos()) {
		cout << "ERROR: No se puede agregar la tupla, la cantidad de campos en valoresTupla no coincide con la cantidad de columnas en la tabla" << endl;
		return ERROR;
	}
	else {
		Cadena vacia("@EMPTY@");
		char delimitador[] = ":";
		bool ok = true;
		int i = 1;
		for (Iterador <Columna> x = columnas->GetIterador(); !x.EsFin(); x++) {
			Cadena aux = valoresTupla.AuxStrtok(i, delimitador);
			if (x.Elemento().GetCalificador() != EMPTY) {
				if (aux == "") { // si encuentra "::" el metodo AuxStrok  devuelve VACIO
					cout << "ERROR: No se puede agregar la tupla, no se puede agregar un dato vacio a una columna que no es EMPTY." << endl;
					return ERROR;
				}
				else {
					if (x.Elemento().GetCalificador() == PK && (x.Elemento().ExisteDto(aux))) {
						cout << "ERROR: No se puede agregar la tupla, la PK ya existe en la tabla." << endl;
						return ERROR;
					}
					else {
						x.ElementoInseguro().AgregarDatos(aux);
					}
				}
			}
			else {
				if (aux == "") {
					x.ElementoInseguro().AgregarDatos(vacia);
				}
				else {
					x.ElementoInseguro().AgregarDatos(aux);
				}
			}
			i++;

		}
	}
	return OK;
}

TipoRetorno Tabla::deleteFrom(Cadena& condicionEliminar) {
	
	if (columnas->EsVacia()) {
		cout << "ERROR: No se puede eliminar la tupla, la columna en condicionEliminar no pertenece a la tabla." << endl;
		return ERROR;
	}
	bool ok = false;
	if (condicionEliminar == "") {
		for (int i = 1; i <= columnas->ElementoPpio().TamanioColumna(); i++) {
			BorrarTupla(i - 1);
			i = 0;
		}
	}
	if (condicionEliminar.Contains("=")) {
		char delimitador[] = "=";
		Cadena nomcol = condicionEliminar.AuxStrtok(1, delimitador);
		Cadena valor = condicionEliminar.AuxStrtok(2, delimitador);
		ok=this->EliminarCondicion(nomcol, 1, valor);
	}
	else {
		if (condicionEliminar.Contains("!")) {
			char delimitador[] = "!";
			Cadena nomcol = condicionEliminar.AuxStrtok(1, delimitador);
			Cadena valor = condicionEliminar.AuxStrtok(2, delimitador);
			ok=this->EliminarCondicion(nomcol, 2, valor);
		}
		else {
			if (condicionEliminar.Contains("<")) {
				char delimitador[] = "<";
				Cadena nomcol = condicionEliminar.AuxStrtok(1, delimitador);
				Cadena valor = condicionEliminar.AuxStrtok(2, delimitador);
				ok=this->EliminarCondicion(nomcol, 3, valor);
			}
			else {
				char delimitador[] = ">";
				Cadena nomcol = condicionEliminar.AuxStrtok(1, delimitador);
				Cadena valor = condicionEliminar.AuxStrtok(2, delimitador);
				ok=this->EliminarCondicion(nomcol, 4, valor);

			}
		}
	}
	if (!ok) {
		cout << "ERROR: No se puede eliminar la tupla, la columna en condicionEliminar no pertenece a la tabla." << endl;
		return ERROR;
	}
	return OK;
}

void Tabla::printMetadata() {
	cout << "Listado de esquema de la tabla " << nombre << ":" << endl << endl;
	if (columnas->EsVacia()) {
		cout << "La tabla no tiene columnas" << endl;
	}
	else {
		for (Iterador <Columna> i = columnas->GetIterador(); !i.EsFin(); i++) {
			if (i.Elemento().GetCalificador() == 2) cout << "*";
			cout << i.Elemento().GetNombre() << endl;
		}
	}
}

void Tabla::printDataTable() {
	cout << "Listado de datos de la tabla " << nombre << ":" << endl;
	cout << "" << endl;

	if (columnas->EsVacia()) {
		cout << "La tabla no tiene Columnas." << endl;
		return;
	}

	else { // IMPRIME NOMBRE DE LAS COLUMNAS

		Iterador <Columna> i = columnas->GetIterador();
		while (!i.EsFin()) {
			if (i.Elemento().GetCalificador() == PK) {
				cout << "*";
			}
			cout << i.Elemento().GetNombre();
			i++;
			if (!i.EsFin()) {
				cout << ":";
			}


		}
		cout << "" << endl;

		if (columnas->ElementoPpio().TamanioColumna() == 0) {
			cout << "La tabla no tiene tuplas." << endl;
		}
		else {
			// IMPRIME LAS TUPLA EN ORDEN ASCENDENTE DE LA COLUMNA PK
			if (this->tienePK) {
				int  colpk = -1;
				bool salir = false;
				ListaOrdImp<Cadena>* datopk = new ListaOrdImp<Cadena>();
				for (Iterador <Columna> i = columnas->GetIterador(); !i.EsFin() && !salir; i++) {
					if (i.Elemento().GetCalificador() == PK) {
						colpk = columnas->Posicion(i.Elemento());
						salir = true;
						for (int c = 1; c <= i.Elemento().TamanioColumna(); c++) {
							datopk->AgregarOrd(i.Elemento().GetDatoenpos(c - 1)); // cargo todos los datos de la columna de forma ordenada en listaordimp
						}
					}
				}
				for (Iterador <Cadena> i = datopk->GetIterador(); !i.EsFin(); i++) {
					Cadena dato = i.Elemento();
					int pos = columnas->ElementoPos(colpk).PosicionDato(dato); // me fijo en que posicion se encuentra en la columna para despues imprimir
																					// toda la tupla			
					Iterador <Columna> clna = columnas->GetIterador();
					while (!clna.EsFin()) {
						cout << clna.Elemento().GetDatoenpos(pos);
						clna++;
						if (!clna.EsFin()) {
							cout << ":";
						}
					}
					cout << "" << endl;

				}
				delete datopk;
			}
			else { // IMPRIME EN ORDEN DE CREACION (LA MAS ANTIGUA PRIMERO)
				for (int x = 1; x <= columnas->ElementoPpio().TamanioColumna(); x++) {
					int contador = 1;
					Iterador <Columna> i = columnas->GetIterador();
					while (!i.EsFin()) {
						cout << i.Elemento().GetDatoenpos(x - 1);
						i++;
						if (!i.EsFin()) {
							cout << ":";
						}
					}
					cout << "" << endl;

				}
			}
		}
	}


}

TipoRetorno Tabla::join(Tabla& t1, Tabla& t2) {
	// NO IMPLEMENTADA
	return NO_IMPLEMENTADA;
}

bool Tabla::ExisteTupla(Cadena& datotupla) const
{
	unsigned int cant = datotupla.CantidadsubCadenas();
	bool ok = false;
	int contador = 0;
	int i = 0;
	char delimitador[] = ":";
	Cadena dato = datotupla.AuxStrtok(1, delimitador);
	if (!columnas->ElementoPpio().ExisteDto(dato)) { // si el primer dato no se encuentra en la primera columna retorna false
		return false;
	}
	ok = false;
	for (int x = 1; x <= columnas->ElementoPpio().TamanioColumna() && !ok; x++) { // voy bajando por las columnas dato a dato
		i = 1;
		while (i <= cant && !ok) {
			dato = datotupla.AuxStrtok(i, delimitador);
			Cadena cdato = columnas->ElementoPos(i - 1).GetDatoenpos(x - 1);

			if (dato == cdato) { // si el dato es igual paso a la siguiente columna sino salgo del while
				i++;
			}
			else {
				ok = true;
			}
			if (i > cant) { // si ya no tengo mas columnas es verdadero
				return true;
			}
		}
	}


	return false;
}

void Tabla::BorraTuplaRepetidas()
{
	bool ok = false;
	Iterador <Columna> i = columnas->GetIterador();
	for (int a = 1; a <= columnas->ElementoPpio().TamanioColumna(); a++) { // me paro en un dato 
		for (int b = a + 1; b <= columnas->ElementoPpio().TamanioColumna(); b++) { // busco en los siguientes dato 
			if (i.Elemento().GetDatoenpos(a - 1) == i.Elemento().GetDatoenpos(b - 1)) {
				if (columnas->CantidadElementos() == 1) {
					BorrarTupla(a - 1);
					a = 0; // vuelvo al inicio de la tabla porque se modifico
				}
				else {
					i++; // avanzo a la siguiente columna
					ok = true;
					while (!i.EsFin() && ok) {
						if (i.Elemento().GetDatoenpos(a - 1) == i.Elemento().GetDatoenpos(b - 1)) {

							i++; // sigo a la siguientes columnas
						}
						else ok = false;
					}
					if (ok) {
						BorrarTupla(a - 1); // borro la primera tupla
						a = 0;
					}
					i.Principio();
				}
			}
		}
	}


}

void Tabla::BorrarTupla(unsigned int pos)
{
	for (Iterador<Columna> i = columnas->GetIterador(); !i.EsFin(); i++) {
		i.ElementoInseguro().BorrarDatoPos(pos);
	}
}

bool Tabla::EliminarCondicion(Cadena& nombclna, unsigned int opr, Cadena& valor)
{
	bool res = false;
	for (Iterador <Columna> i = columnas->GetIterador(); !i.EsFin(); i++) {
		if (i.Elemento().GetNombre() == nombclna) { 
			switch (opr) {
			case 1: // =
				for (int n = 1; n <= i.Elemento().TamanioColumna(); n++) {
					if (i.Elemento().GetDatoenpos(n - 1) == valor) {
						BorrarTupla(n - 1);
						n = 0;
					}
				}
				break;
			case 2:  // !
				for (int n = 1; n <= i.Elemento().TamanioColumna(); n++) {
					if (i.Elemento().GetDatoenpos(n - 1) != valor) {
						BorrarTupla(n - 1);
						n = 0;
					}
				}
				break;
			case 3: // <
				for (int n = 1; n <= i.Elemento().TamanioColumna(); n++) {
					if (i.Elemento().GetDatoenpos(n - 1) < valor) {
						BorrarTupla(n - 1);
						n = 0;
					}
				}
				break;
			case 4: // >
				for (int n = 1; n <= i.Elemento().TamanioColumna(); n++) {
					if (i.Elemento().GetDatoenpos(n - 1) > valor) {
						BorrarTupla(n - 1);
						n = 0;
					}
				}
				break;

			}
			res = true;
		}
	}
	return res;
}

#endif
