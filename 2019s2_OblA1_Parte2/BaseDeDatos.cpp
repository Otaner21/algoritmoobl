#include "BaseDeDatos.h"

#ifndef BASEDEDATOS_CPP
#define BASEDEDATOS_CPP


BaseDeDatos::BaseDeDatos() {
	//tablas = new ListaOrdImp<Tabla>(); 
	assert(false);
}

BaseDeDatos::BaseDeDatos(unsigned int MAX_MODIFICADAS) {
	tablas = new ListaOrdImp<Tabla>();
}

BaseDeDatos::BaseDeDatos(const BaseDeDatos& bd) {
	assert(false);
}

BaseDeDatos::~BaseDeDatos() {
	delete tablas;
}

BaseDeDatos& BaseDeDatos::operator=(const BaseDeDatos& bd) {
	if (this != &bd) {
		assert(false);
	}
	return *this;
}

TipoRetorno BaseDeDatos::createTable(Cadena nombreTabla)
{
	if (tablas->Existe(nombreTabla)) {
		cout << "ERROR: No se puede crear la tabla, el nombre ya existe." << endl;
		return ERROR;
	}
	Tabla table(nombreTabla);
	tablas->AgregarOrd(table);
	return OK;
}

TipoRetorno BaseDeDatos::dropTable(Cadena nombreTabla)
{
	if (tablas->EsVacia() || !tablas->Existe(nombreTabla)) {
		cout << "ERROR: No se puede eliminar la tabla, el nombre no existe." << endl;
		return ERROR;


	}
	else {
		//PREGUNTAR CUAL DE LAS 2 FORMAS
		//tablas->Borrar(nombreTabla);
		Tabla table = tablas->RecuperarInseguro(nombreTabla);
		tablas->Borrar(table);
		return OK;
	}
}

TipoRetorno BaseDeDatos::addCol(Cadena nombreTabla, Cadena nombreCol, CalifCol calificadorColumna)
{
	if (tablas->EsVacia()) {
		cout << "ERROR: No se puede agregar la columna, nombreTabla no existe." << endl;
		return ERROR;
	}
	if (tablas->Existe(nombreTabla)) {
		//PREGUNTAR CUAL DE LAS 2 FORMAS ESTA BIEN
		return tablas->RecuperarInseguro(nombreTabla).addCol(nombreCol, calificadorColumna);
		//Tabla aux = tablas->RecuperarInseguro(nombreTabla);
		//return aux.addCol(nombreCol, calificadorColumna);
		//return OK;
	}
	else {
		cout << "ERROR: No se puede agregar la columna, nombreTabla no existe." << endl;
		return ERROR;
	}



	return OK;
}

TipoRetorno BaseDeDatos::dropCol(Cadena nombreTabla, Cadena nombreCol)
{
	if (tablas->EsVacia() || !tablas->Existe(nombreTabla)) {
		cout << "ERROR: No se puede eliminar la columna, nombreTabla no existe." << endl;
		return	ERROR;
	}

	return tablas->RecuperarInseguro(nombreTabla).delCol(nombreCol);
}

TipoRetorno BaseDeDatos::insertInto(Cadena nombreTabla, Cadena valoresTupla)
{

	if (tablas->EsVacia() || !tablas->Existe(nombreTabla)) {
		cout << "ERROR: No se puede agregar la tupla, nombreTabla no existe." << endl;
		return ERROR;
	}
	else {
		Tabla& aux = tablas->RecuperarInseguro(nombreTabla);
		return aux.insertInto(valoresTupla);
	}





	//return OK;
}

TipoRetorno BaseDeDatos::deleteFrom(Cadena nombreTabla, Cadena condicionEliminar)
{
	if (tablas->EsVacia() || !tablas->Existe(nombreTabla)) {
		cout << "ERROR: No se puede eliminar la tupla, nombreTabla no existe." << endl;
		return ERROR;
	}
	else {
		//PREGUNTAR SI ESTA BIEN ASI EL RETURN
		return tablas->RecuperarInseguro(nombreTabla).deleteFrom(condicionEliminar);
	}



	
}

TipoRetorno BaseDeDatos::printTables()
{
	cout << "Listado de tablas:" << endl << endl;
	if (tablas->EsVacia()) {
		cout << "No hay tablas creadas"; cout << endl;
	}
	else {
		for (Iterador <Tabla> i = tablas->GetIterador(); !i.EsFin(); i++) {
			cout << i.Elemento().GetNombre() << endl;
		}
	}
	return OK;
}

TipoRetorno BaseDeDatos::printMetadata(Cadena nombreTabla)
{
	if (tablas->Existe(nombreTabla)) {
		Tabla aux = tablas->Recuperar(nombreTabla);
		aux.printMetadata();
		return OK;
	}
	else {
		cout << "ERROR: No se puede imprimir el esquema, nombreTabla no existe." << endl;
		return ERROR;
	}
	//NO_IMPLEMENTADA;
}

TipoRetorno BaseDeDatos::printDataTable(Cadena nombreTabla)
{
	if (tablas->EsVacia()) {
		cout << "ERROR: No se pueden imprimir los datos, nombreTabla no existe.";
		return ERROR;
	}
	if (tablas->Existe(nombreTabla)) {
		Tabla  aux = tablas->Recuperar(nombreTabla);
		aux.printDataTable();
	}
	else {
		cout << "ERROR: No se pueden imprimir los datos, nombreTabla no existe.";
		return ERROR;
	}
	// NO IMPLEMENTADA
	return OK;
}

TipoRetorno BaseDeDatos::join(Cadena nombreTabla1, Cadena nombreTabla2, Cadena nombreTabla3)
{
	// NO IMPLEMENTADA
	return NO_IMPLEMENTADA;
}

TipoRetorno BaseDeDatos::recent()
{
	// NO IMPLEMENTADA
	return NO_IMPLEMENTADA;
}


#endif
