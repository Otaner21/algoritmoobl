#ifndef COLUMNA_H
#define COLUMNA_H
#include "Constantes.h"
#include "Cadena.h"
#include "ListaOrd.h"
#include "ListaOrdImp.h"
#include "CalifCol.h"
#include "ListaPos.h"
#include "ListaPosImp.h"

 
class Columna;
ostream& operator<<(ostream& out, const Columna& clna);

class Columna
{
public :
	Columna();
	Columna(Cadena nombreCol, CalifCol calif);
	Columna(const Columna& clna);

	virtual ~Columna();

	Columna& operator = (const Columna &clna);
	
	bool operator == (const Columna clna) const;
	bool operator<=(const Columna & clna) const;
	bool operator>=(const Columna &c) const;
	bool operator!=(const Columna& c) const;
	bool operator<(const Columna& c) const;
	bool operator>(const Columna& c) const;

	bool ExisteDto(const Cadena& cdna) const;
	
	const Cadena& GetNombre()const;
	const CalifCol& GetCalificador()const;
	const Cadena& GetDatoenpos(unsigned int pos) const;
	unsigned int PosicionDato(const Cadena& cdna)const;
	unsigned int VecesExiste(const Cadena& cdna) const;

	unsigned int TamanioColumna() const;

	void AgregarDatos(Cadena& dato);
	void BorrarDatoPos(unsigned int pos);
	


private : 
	Cadena nombre;
	CalifCol calificador;
	ListaPos<Cadena>* datos;

};
#endif
