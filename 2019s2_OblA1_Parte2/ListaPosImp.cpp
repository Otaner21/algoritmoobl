#include "ListaPosImp.h"

#ifndef LISTAPOSIMP_CPP
#define LISTAPOSIMP_CPP

template <class T>
inline ostream & operator<<(ostream &out, const ListaPosImp<T> &l)
{
	l.Imprimir(out);
	return out;
}

template <class T>
ListaPos<T>* ListaPosImp<T>::CrearVacia() const
{
	return new ListaPosImp<T>();
}

template <class T>
ListaPosImp<T>::ListaPosImp()
{
	this->fin = NULL;
	this->ppio = NULL;
	this->largo = 0;
	// NO IMPLEMENTADA
}

template <class T>
ListaPosImp<T>::ListaPosImp(const ListaPos<T> &l)
{
	this->fin = NULL;
	this->ppio = NULL;
	this->largo = 0;

	*this = l;
	// NO IMPLEMENTADA
}

template <class T>
ListaPosImp<T>::ListaPosImp(const ListaPosImp<T> &l)
{
	this->fin = NULL;
	this->ppio = NULL;
	this->largo = 0;

	*this = l;	
	// NO IMPLEMENTADA
}

template <class T>
ListaPos<T>& ListaPosImp<T>::operator=(const ListaPos<T> &l)
{
	if (this != &l) {
		// NO IMPLEMENTADA
		this->Vaciar();

		for (Iterador<T> i = l.GetIterador(); !i.EsFin();) {
			AgregarFin(i++);
		}
	}
	return *this;
}

template <class T>
ListaPos<T>& ListaPosImp<T>::operator=(const ListaPosImp<T> &l)
{
	if (this != &l) {
		// NO IMPLEMENTADA
		this->Vaciar();

		for (Iterador<T> i = l.GetIterador(); !i.EsFin();) {
			AgregarFin(i++);
		}
	}
	return *this;
}

template <class T>
ListaPosImp<T>::~ListaPosImp()
{
	// NO IMPLEMENTADA
	this->Vaciar();
}

template <class T>
void ListaPosImp<T>::AgregarPpio(const T &e) 
{
	NodoLista<T> *nuevo = new NodoLista<T>(e, NULL, ppio);
	if (fin == NULL)
		fin = nuevo;
	else
		ppio->ant = nuevo;
	ppio = nuevo;
	largo++;
	// NO IMPLEMENTADA
}

template <class T>
void ListaPosImp<T>::AgregarFin(const T &e) 
{
	NodoLista<T> *nuevo = new NodoLista<T>(e, fin, NULL);
	if (!ppio)
		ppio = nuevo;
	else
		fin->sig = nuevo;
	fin = nuevo;
	largo++;
	// NO IMPLEMENTADA
}

template <class T>
void ListaPosImp<T>::AgregarPos(const T &e, unsigned int pos)
{
	if (pos == 0) AgregarPpio(e);
	else if (pos >= this->largo) AgregarFin(e);
	else{
		NodoLista<T> *aux = ppio;
		while (pos > 1) {
			aux = aux->sig;
			pos--;
		}
		NodoLista<T> *nuevo = new NodoLista<T>(e, aux, aux->sig);
		aux->sig->ant = nuevo;
		aux->sig = nuevo;
		largo++;
	}
	// NO IMPLEMENTADA
}

template <class T>
void ListaPosImp<T>::BorrarPpio()
{
	if (ppio == NULL) return;
	NodoLista<T> *borrar = ppio;
	if(ppio ->sig != NULL)
		ppio = ppio->sig;
	else ppio = NULL;
	delete borrar;
	largo--;
	if (ppio != NULL)
		ppio->ant = NULL;
	else
		fin = NULL;
	// NO IMPLEMENTADA
}

template <class T>
void ListaPosImp<T>::BorrarFin()
{
	if (fin == NULL)
		return;

	NodoLista<T> *borrar = fin;
	fin = fin->ant;
	delete borrar;
	largo--;

	if (fin != NULL)
		fin->sig = NULL;
	else
		ppio = NULL;
	// NO IMPLEMENTADA
}

template <class T>
void ListaPosImp<T>::BorrarPos(unsigned int pos)
{
	if (pos >= this->largo) return;
	if (pos == 0) BorrarPpio();
	else {
		NodoLista<T> *aux = ppio;
		while (pos > 1) {
			aux = aux->sig;
			pos--;
		}
		NodoLista<T> *aBorrar = aux->sig;
		aux->sig = aux->sig->sig;
		if (aux->sig != NULL) {
			aux->sig->ant = aux;
		}
		delete aBorrar;
		largo--;
	}
	// NO IMPLEMENTADA
}

template <class T>
void ListaPosImp<T>::Borrar(const T &e)
{
	if (ppio == NULL) return;
	if (e == ppio->dato) BorrarPpio();
	else if (e == fin->dato) BorrarFin();
	else BorrarAux(ppio, e);
	// NO IMPLEMENTADA
}

template <class T>
void ListaPosImp<T>::BorrarAux(NodoLista<T> *nodo, const T &e)
{
	if (nodo == NULL) return;

	if (nodo->dato == e) {
		if(nodo->sig!=NULL) nodo->sig->ant = nodo->ant;
		nodo->ant->sig = nodo->sig;
		delete nodo;
		largo--;
	}else if(nodo != NULL && nodo->sig!=NULL) BorrarAux(nodo->sig, e);
}

template <class T>
T& ListaPosImp<T>::ElementoPpio() const
{
	// NO IMPLEMENTADA
	assert(ppio != NULL);
	return ppio->dato;
}

template <class T>
T& ListaPosImp<T>::ElementoFin() const
{
	// NO IMPLEMENTADA
	assert(fin != NULL);
	return fin->dato;
}

template <class T>
T& ListaPosImp<T>::ElementoPos(unsigned int pos) const
{
	NodoLista<T>* aux = ppio;
	while (pos > 0) {
		aux = aux->sig;
		pos--;
	}
	return aux->dato;
	// NO IMPLEMENTADA
	
}

template <class T>
unsigned int ListaPosImp<T>::Posicion(const T &e) const
{
	// NO IMPLEMENTADA
	NodoLista<T> *aux = ppio;
	int cont = 0;
	while (aux->dato != e) {
		aux = aux->sig;
		cont++;
	}
	return cont;
}

template <class T>
bool ListaPosImp<T>::Existe(const T &e) const
{
	// NO IMPLEMENTADA
	if (EsVacia()) return false;
	NodoLista<T> *aux = ppio;
	while (aux != NULL) {
		if (aux->dato == e) return true;
		aux = aux->sig;
	}
	return false;
}

template <class T>
void ListaPosImp<T>::Vaciar()
{
	// NO IMPLEMENTADA
	NodoLista<T> *aux = ppio;
	while (aux != NULL) {
		NodoLista<T> *borrar = aux;
		aux = aux->sig;
		delete borrar;
	}

	this->fin = NULL;
	this->ppio = NULL;
	this->largo = 0;
}

template <class T>
unsigned int ListaPosImp<T>::CantidadElementos() const
{ 
	// NO IMPLEMENTADA
	return largo;
}

template <class T>
bool ListaPosImp<T>::EsVacia() const
{
	// NO IMPLEMENTADA
	return largo == 0;
}

template <class T>
bool ListaPosImp<T>::EsLlena() const
{
	// NO IMPLEMENTADA
	return false;
}

template <class T>
ListaPos<T>* ListaPosImp<T>::Clon() const
{
	// NO IMPLEMENTADA
	ListaPos<T>* clon = new ListaPosImp<T>();

	for (Iterador<T> i = this->GetIterador(); !i.EsFin();)
		clon->AgregarFin(i++);

	return clon;
}

template <class T>
Iterador<T> ListaPosImp<T>::GetIterador() const
{
	return IteradorListaPosImp<T>(*this);
}

template <class T>
void ListaPosImp<T>::Imprimir(ostream& o) const
{
	// NO IMPLEMENTADA
	// en luegar de hacer cout << ... poner o << ...
	for (Iterador<T> i = GetIterador(); !i.EsFin();) {
		o << i++;
		if (!i.EsFin()) o << " ";
	}
}

#endif